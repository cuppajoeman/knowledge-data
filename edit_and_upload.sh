#!/bin/bash

# Input is a name of the file without the extension
nvim $1.tex

# Compile it to pdf
latexmk -pdf $1.tex

# Remove excess files
sh ./cleaner.sh

# Update kg_base tables
python3 kgbase_data/update_kg_table.py && echo tables updated

# Refresh arguments file
sh ./kgbase_data/reset_argfile.sh

# open tables and arguments file
nvim -O kgbase_data/arguments.txt kgbase_data/tables.txt

# push data to online
python3 content_to_knowledge_graph.py $1

# push to github
git add -A && git commit && git push
